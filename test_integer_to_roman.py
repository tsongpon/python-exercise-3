import unittest

from integer_to_roman import IntToRomanConverter


class TestIntToRomanConverter(unittest.TestCase):
    def test_int_to_roman(self):
        converter = IntToRomanConverter()
        self.assertEqual('I', converter.int_to_roman(1))
        self.assertEqual('III', converter.int_to_roman(3))
        self.assertEqual('IV', converter.int_to_roman(4))
        self.assertEqual('IX', converter.int_to_roman(9))
        self.assertEqual('XXX', converter.int_to_roman(30))
        self.assertEqual('L', converter.int_to_roman(50))
        self.assertEqual('XL', converter.int_to_roman(40))
        self.assertEqual('XLIX', converter.int_to_roman(49))
        self.assertEqual('MCMXCVIII', converter.int_to_roman(1998))
        self.assertEqual('MCMXCIX', converter.int_to_roman(1999))


if __name__ == '__main__':
    unittest.main()
