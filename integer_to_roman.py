class IntToRomanConverter:
    def __init__(self):
        self.dec_values = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
        self.roman_symbol = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]

    def int_to_roman(self, num):
        roman_num = ''
        i = 0
        while num > 0:
            for _ in range(num // self.dec_values[i]):
                roman_num += self.roman_symbol[i]
                num -= self.dec_values[i]
            i += 1
        return roman_num
